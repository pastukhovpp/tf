<powershell>

start-transcript "env:APPDATA\transcript.log"
$dl_terminal - start-job -scriptblock {
    (New-Object System.Net.WebClient).DownloadFile("${terinal_url}"),
        "$env:APPDATA\terminal.zip")
}
$cwa_path = "$env:APPDATA\amazon-cloudwatch-agent.msi"
$dl_and_install_cwa_job = start-job -scriptblock {
    (New-Object System.Net.WebClient).DownloadFile("${cwa_url", $cwa_path);
}

$dl_terminal | wait-job
set-location $env:APPDATAexpand-archive -Path 'terminal.zip'
Set-Location.\terminal\terminal

.\create_config.ps1 -Login "$login" `
-Password "${password}" `
-Server "${server}" `
-DataServer "${dataserver}" `
-Symbol "${symbol}"

start-process -filepath "terminal.exe" -ArgumentList "config\config.ini"

$cw_config = "${cw_config}"
$cw_config_path = "$env:APPDATA\CloudWatch-config.json"
[IO.File]::WriteAllBytes($cw_config_path, [Convert]::FromBase64String($cw_config))
$dl_and_install_cwa_job | wait-job
& "C:\Program Files\Amazon\AmazonCloudWatchAgent\amazon-cloudwatch-agent-ctl.ps1" -a fetch-config -m ec2 -s -c file:$cw_config_path

stop-transcript

</powershell>