//aws
variable "profile"      { sensitive = true }
variable "access_key"   { sensitive = true }
variable "secret_key"   { sensitive = true }

//general
variable "region"       { default = "us-east-2" }
variable "cwa_url"      { default = "https://s3.amazonaws.com/amazoncloudwatch-agent/windows/amd64/latest/amazon-cloudwatch-agent.msi" }
variable "target_capacity" {}

//mt4
variable "mt4_login"       {}
variable "mt4_password"    { sensitive = true }
variable "mt4_server"      {}
variable "mt4_dataserver"  {}
variable "mt4_symbol"      {}
variable "mt4_terminal_url"{}

variable "amis"       {
    default = {
        us-east-2 = {
            linux = "ami-0a0ad6b70e61be944"
            windows = "ami-0d5b55fd8cd8738f5"
        }
    }
}

variable "iam_policy_arn" { default = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore", // use Session Manager
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"] } // stream local logs to CW