provider "aws" {
	profile = var.profile
	access_key = var.access_key
	region = var.region
}

resource "aws_iam_role" "role_tf" {
	name = "role_tf"
	assume_role_policy = file("EC2_rolepolicy.json")
}

resource "aws_iam_role" "tf_fleet_role" {
	name = "tf_fleet_role"
	assume_role_policy = file("SpotFleet_rolepolicy.json")
}

resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
	role = aws_iam_role.role_tf.name
	count = length(var.iam_policy_arn)
	policy_arn = var.iam_policy_arn[count.index]
}

resource "aws_iam_role_policy_attachment" "tf_fleet_role_attachment" {
	role = aws_iam_role.tf_fleet_role.name
	policy_arn = "arn:aws:iam:aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
}

resource "aws_iam_instance_profile" "tf_profile" {
	name = "tf_profile"
	role = aws_iam_role.role_tf.name
}

resource "aws_launch_template" "tf_template" {
	image_id = var.amis[var.region].windows
	instance_type = "t2.micro"
	instance_market_options {
		market_type = "spot"
	}
	user_data = base64encode(templatefile("${path.module}/bootstrap.ps1", {
		"cwa_url" = var.cwa_url,
		"terminal_url" = var.mt4_terminal_url,
		"login" = var.mt4_login,
		"password" = var.mt4_password,
		"server" = var.mt4_server,
		"dataserver" = var.mt4_dataserver,
		"symbol" = var.mt4_symbol,
		"cw_config" = base64encode(file("${path.module}/CloudWatch-config.json"))
	}))
	instance_initiated_shutdown_behavior = "terminate"
	iam_instance_profile {
		arn = aws_iam_instance_profile.tf_profile.arn
	}
}

resource "aws_spot_fleet_request" "tf_spot_request" {
	iam_fleet_role = aws_iam_role.tf_fleet_role.arn
	target_capacity = var.target_capacity
	wait_for_fulfillment = true
	allocation-strategy = "lowestPrice"
	terminate_instances_with_expiration = true
	launch_template_config {
		launch_template_specification {
			id = aws_launch_template.tf_template.id
			version = aws_launch_template.tf_template.latest_verskon
		}
	}
}

/*
.\t validate
.\ta
.\td
*/