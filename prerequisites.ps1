wget -O terraform.zip https://releases.hashicorp.com/terraform/0.14.4/terraform_0.14.4_windows_amd64.zip
expand-archive -Path 'terraform.zip' -DestinationPath (get-location).path
Remove-Item terraform.zip
.\terraform init